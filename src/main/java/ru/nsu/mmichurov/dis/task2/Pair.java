package ru.nsu.mmichurov.dis.task2;

import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(staticName = "of")
@EqualsAndHashCode
public class Pair<T1, T2> {
    public final T1 first;
    public final T2 second;
}
