package ru.nsu.mmichurov.dis.task2;

import lombok.experimental.UtilityClass;
import lombok.extern.log4j.Log4j2;
import lombok.val;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.jetbrains.annotations.NotNull;
import ru.nsu.g.mmichurov.jcli.ArgType;
import ru.nsu.g.mmichurov.jcli.ArgumentParser;
import ru.nsu.mmichurov.dis.task2.db.DatabaseUtility;
import ru.nsu.mmichurov.dis.task2.db.inserters.INodeInserter;
import ru.nsu.mmichurov.dis.task2.db.inserters.InsertMethod;
import ru.nsu.mmichurov.dis.task2.db.inserters.NodeInserterFactory;
import ru.nsu.mmichurov.dis.task2.db.inserters.SchemaType;

import jakarta.xml.bind.JAXBException;

import javax.xml.stream.XMLStreamException;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Locale;
import java.util.function.Function;

@UtilityClass
@Log4j2
public class Main {
    private static final String APP_NAME = "task2";

    private static final int BUFFER_SIZE = 1024 * 1024 * 10;
    private static final int DEFAULT_BATCH_SIZE = 100;

    static {
        Locale.setDefault(Locale.US);
    }

    public static void main(final @NotNull String[] args) {
        val options = Options.parse(args);

        if (!options.db.tryAll) {
            run(options);
            return;
        }

        for (val schemaType : SchemaType.values()) {
            for (val insertMethod : InsertMethod.values()) {
                options.db.schemaType = schemaType;
                options.db.insertMethod = insertMethod;

                run(options);
            }
        }
    }

    private static void run(final @NotNull Options options) {
        try (val connection = DriverManager.getConnection(
                options.db.connectionString,
                options.db.user,
                options.db.password
        )) {
            try (
                    val rawInput = new FileInputStream(options.fileName);
                    val xmlInput = new BZip2CompressorInputStream(new BufferedInputStream(rawInput, BUFFER_SIZE));
                    val unmarshaller = new NodeUnmarshaller(xmlInput)
            ) {
                connection.setAutoCommit(false);

                if (options.db.create) {
                    DatabaseUtility.createDatabaseStructure(connection);
                    connection.commit();
                }

                val inserterFactory = new NodeInserterFactory(options.db.batchSize);
                val inserter = inserterFactory.getInserter(options.db.insertMethod, options.db.schemaType);

                val statistics = insertData(
                        connection,
                        unmarshaller,
                        inserter,
                        startTime -> makeContinueFunction(
                                startTime, options.timeLimit, options.nodesCountLimit, options.showProgress
                        )
                );

                var statisticsMessage = String.format(
                        "Inserted %d nodes as %s in %.2fs (%.2f nodes/s) using %s method",
                        statistics.totalInserted, options.db.schemaType, statistics.elapsed,
                        statistics.speed, options.db.insertMethod
                );
                if (InsertMethod.BATCH == options.db.insertMethod) {
                    statisticsMessage += String.format(" (batch size %d)", options.db.batchSize);
                }
                log.info(statisticsMessage);
                System.out.println(statisticsMessage);
            } catch (Exception e) {
                connection.rollback();
                throw e;
            }
        } catch (
                SQLException
                        | IOException
                        | XMLStreamException
                        | JAXBException
                        | NodeInserterFactory.InserterNotImplementedException e
        ) {
            log.error(e);
        }
    }

    private static @NotNull Statistics insertData(
            final @NotNull Connection connection,
            final @NotNull NodeUnmarshaller unmarshaller,
            final @NotNull INodeInserter inserter,
            final @NotNull Function<Long, Function<Long, Boolean>> continueFunctionProvider
    ) throws SQLException, NodeInserterFactory.InserterNotImplementedException {
        log.debug("Cleaning tables");
        inserter.clean(connection);

        val startTime = System.nanoTime();

        log.debug("Inserting data");
        val totalInserted = inserter.insert(
                unmarshaller,
                connection,
                continueFunctionProvider.apply(startTime)
        );

        connection.commit();

        val elapsed = (System.nanoTime() - startTime) / 1e9d;

        return new Statistics(totalInserted, elapsed, totalInserted / elapsed);
    }

    private static final class Statistics {
        public final long totalInserted;
        public final double elapsed;
        public final double speed;

        private Statistics(long totalInserted, double elapsed, double speed) {
            this.totalInserted = totalInserted;
            this.elapsed = elapsed;
            this.speed = speed;
        }
    }

    private static @NotNull Function<Long, Boolean> makeContinueFunction(
            final long startTimeNs,
            final long timeLimitS,
            final long countLimit,
            final boolean showProgress
    ) {
        val timeLimitNs = timeLimitS * 1e9;

        if (!showProgress) {
            return count -> count < countLimit && System.nanoTime() - startTimeNs < timeLimitNs;
        }

        return count -> {
            val elapsedNs = System.nanoTime() - startTimeNs;

            if (count % 250 == 0) {
                System.out.printf("\r%ds Inserted %d nodes", (int) (elapsedNs / 1e9d), count);
            }

            val shouldContinue = count < countLimit && elapsedNs < timeLimitNs;

            if (!shouldContinue) {
                System.out.print("\r                                                           \r");
            }

            return shouldContinue;
        };
    }

    private static final class Options {
        public @NotNull String fileName = "";
        public long nodesCountLimit = Long.MAX_VALUE;
        public long timeLimit = Long.MAX_VALUE / 1_000_000_000;
        public boolean showProgress = false;
        public @NotNull DbOptions db = new DbOptions();

        public static @NotNull Options parse(final @NotNull String... args) {
            val options = new Options();

            val parser = new ArgumentParser(APP_NAME, ArgumentParser.OptionPrefixStyle.GNU);
            val fileName = parser
                    .argument(ArgType.String, "input", "Input file");
            val timeLimit = parser
                    .option(ArgType.Integer, "time-limit", null, "Time limit in seconds")
                    .setDefault(Integer.MAX_VALUE);
            val nodesCountLimit = parser
                    .option(ArgType.Integer, "count-limit", null, "Inserted nodes count limit")
                    .setDefault(Integer.MAX_VALUE);
            val showProgress = parser
                    .option(ArgType.Boolean, "show-progress", null, "Dynamically show number of nodes inserted (may reduce performance)")
                    .setDefault(false);

            val databaseUrl = parser
                    .option(ArgType.String, "db-url", null, "Database connection string")
                    .required();
            val user = parser
                    .option(ArgType.String, "user", "u", "Database username")
                    .required();
            val password = parser
                    .option(ArgType.String, "password", "p", "Database password")
                    .required();
            val insertMethod = parser
                    .option(ArgType.choiceOf(InsertMethod.class), "insert-method", null, "Insert method")
                    .setDefault(InsertMethod.DEFAULT);
            val schemaType = parser
                    .option(ArgType.choiceOf(SchemaType.class), "schema-type", null, "Schema type")
                    .setDefault(SchemaType.RELATIONS);
            val batchSize = parser
                    .option(ArgType.Integer, "batch-size", null, "Batch size for 'batch' insert method")
                    .setDefault(DEFAULT_BATCH_SIZE);
            val create = parser
                    .option(ArgType.Boolean, "create", null, "Create required tables")
                    .setDefault(false);
            val tryAll = parser
                    .option(ArgType.Boolean, "try-all", null, "Try all combinations of schema type and insert method")
                    .setDefault(false);

            parser.parse(args);

            options.fileName = fileName.getValue();
            options.nodesCountLimit = nodesCountLimit.getValue();
            options.timeLimit = timeLimit.getValue();
            options.showProgress = showProgress.getValue();

            options.db.connectionString = databaseUrl.getValue();
            options.db.user = user.getValue();
            options.db.password = password.getValue();
            options.db.insertMethod = insertMethod.getValue();
            options.db.schemaType = schemaType.getValue();
            options.db.batchSize = batchSize.getValue();
            options.db.create = create.getValue();
            options.db.tryAll = tryAll.getValue();

            return options;
        }

        public static final class DbOptions {
            public @NotNull String connectionString = "";
            public @NotNull String user = "";
            public @NotNull String password = "";
            public @NotNull InsertMethod insertMethod = InsertMethod.DEFAULT;
            public @NotNull SchemaType schemaType = SchemaType.RELATIONS;
            public int batchSize = 0;
            public boolean create;
            public boolean tryAll;
        }
    }
}
