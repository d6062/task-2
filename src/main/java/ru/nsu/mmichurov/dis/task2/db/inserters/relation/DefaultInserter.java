package ru.nsu.mmichurov.dis.task2.db.inserters.relation;

import lombok.experimental.UtilityClass;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.openstreetmap.osm.Node;
import org.openstreetmap.osm.Tag;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.MessageFormat;
import java.util.function.Function;

import static ru.nsu.mmichurov.dis.task2.db.DatabaseUtility.escape;

public final class DefaultInserter extends RelationInserter {
    @Override
    public long insert(
        final @NotNull Iterable<Node> nodes,
        final @NotNull Connection connection,
        final @NotNull Function<Long, Boolean> shouldContinue
    ) throws SQLException {
        var insertedCount = 0L;

        try (val statement = connection.createStatement()) {
            for (val node : nodes) {
                if (!shouldContinue.apply(insertedCount)) {
                    break;
                }

                insertNode(node, statement);
                insertedCount += 1;
            }
        }

        return insertedCount;
    }

    private static void insertTag(
        final @NotNull Tag tag,
        final long nodeId,
        final @NotNull Statement statement
    ) throws SQLException {
        val query = QueryFormatter.makeInsert(tag, nodeId, TAG_TABLE);
        statement.execute(query);
    }

    private static void insertNode(
        final @NotNull Node node,
        final @NotNull Statement statement
    ) throws SQLException {
        var query = QueryFormatter.makeInsert(node.getUid().longValue(), node.getUser(), USER_TABLE);
        statement.execute(query);

        query = QueryFormatter.makeInsert(node, NODE_TABLE);
        statement.execute(query);

        for (val tag : node.getTag()) {
            insertTag(tag, node.getId().longValue(), statement);
        }
    }

    @UtilityClass
    private static final class QueryFormatter {
        public static @NotNull String makeInsert(
            final @NotNull Tag tag,
            final long nodeId,
            final @NotNull String table
        ) {
            return MessageFormat.format(
                "insert into {0} values ({1,number,#}, ''{2}'', ''{3}'')",
                table,
                nodeId, escape(tag.getK()), escape(tag.getV())
            );
        }

        public static @NotNull String makeInsert(
            final @NotNull Node node,
            final @NotNull String table
        ) {
            return MessageFormat.format(
                "insert into {0} values ({1,number,#}, {2,number,#}, " +
                    "''{3}'', {4,number,#}, {5,number,#}, {6,number,#}, {7,number,#})",
                table,
                node.getId(), node.getVersion(), node.getTimestamp(),
                node.getUid(), node.getChangeset(),
                node.getLat(), node.getLon()
            );
        }

        public static @NotNull String makeInsert(
            final long userId,
            final @NotNull String userName,
            final @NotNull String table
        ) {
            return MessageFormat.format(
                "insert into {0} values ({1,number,#}, ''{2}'') on conflict do nothing",
                table,
                userId, userName
            );
        }
    }
}
