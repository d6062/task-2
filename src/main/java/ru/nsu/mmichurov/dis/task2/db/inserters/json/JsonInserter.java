package ru.nsu.mmichurov.dis.task2.db.inserters.json;

import ru.nsu.mmichurov.dis.task2.db.inserters.INodeInserter;

import java.util.List;


public abstract class JsonInserter implements INodeInserter {
    protected static final String NODE_TABLE = "osm_json.node";
    protected static final String USER_TABLE = "osm_json.user";

    @Override
    public final Iterable<String> getTables() {
        return List.of(NODE_TABLE, USER_TABLE);
    }
}
