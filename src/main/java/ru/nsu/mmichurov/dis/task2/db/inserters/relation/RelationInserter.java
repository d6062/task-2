package ru.nsu.mmichurov.dis.task2.db.inserters.relation;

import ru.nsu.mmichurov.dis.task2.db.inserters.INodeInserter;

import java.util.List;

public abstract class RelationInserter implements INodeInserter {
    protected static final String NODE_TABLE = "osm.node";
    protected static final String TAG_TABLE = "osm.tag";
    protected static final String USER_TABLE = "osm.user";

    @Override
    public final Iterable<String> getTables() {
        return List.of(TAG_TABLE, NODE_TABLE, USER_TABLE);
    }
}
