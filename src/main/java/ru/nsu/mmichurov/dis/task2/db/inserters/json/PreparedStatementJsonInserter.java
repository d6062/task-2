package ru.nsu.mmichurov.dis.task2.db.inserters.json;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.openstreetmap.osm.Node;
import ru.nsu.mmichurov.dis.task2.db.inserters.json.utility.JsonSerializer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.function.Function;

public final class PreparedStatementJsonInserter extends JsonInserter {
    private final long batchSize;
    private long currentBatchSize = 0;

    public PreparedStatementJsonInserter(final long batchSize) {
        this.batchSize = batchSize;
    }

    public PreparedStatementJsonInserter() {
        this(-1);
    }

    @Override
    public long insert(
        final @NotNull Iterable<Node> nodes,
        final @NotNull Connection connection,
        final @NotNull Function<Long, Boolean> shouldContinue
    ) throws SQLException {
        var insertedCount = 0L;

        try (
                val insertNode = StatementFormatter.makeInsertNode(connection);
                val insertUser = StatementFormatter.makeInsertUser(connection)
        ) {
            for (val node : nodes) {
                if (!shouldContinue.apply(insertedCount)) {
                    break;
                }

                insertNode(node, insertNode, insertUser);
                insertedCount += 1;

                handlePrepared(insertNode, insertUser);
            }

            insertNode.executeBatch();
            insertUser.executeBatch();
        }

        return insertedCount;
    }

    private void insertNode(
        final @NotNull Node node,
        final @NotNull PreparedStatement insertNode,
        final @NotNull PreparedStatement insertUser
    ) throws SQLException {
        StatementFormatter.setArguments(insertUser, node.getUid().longValue(), node.getUser());
        handlePrepared(insertUser);

        StatementFormatter.setArguments(insertNode, node);
        handlePrepared(insertNode);
    }

    private void handlePrepared(final @NotNull PreparedStatement statement) throws SQLException {
        if (batchSize <= 0) {
            statement.execute();
            return;
        }

        statement.addBatch();
        currentBatchSize += 1;
    }

    private void handlePrepared(
        final @NotNull PreparedStatement insertNode,
        final @NotNull PreparedStatement insertUser
    ) throws SQLException {
        if (batchSize <= 0 || currentBatchSize < batchSize) {
            return;
        }

        insertUser.executeBatch();
        insertNode.executeBatch();

        currentBatchSize = 0;
    }

    private static final class StatementFormatter {
        public static @NotNull PreparedStatement makeInsertNode(
            final @NotNull Connection connection
        ) throws SQLException {
            return connection.prepareStatement(
                String.format("insert into %s values(?, ?, ?, ?, ?, ?, ?, ?::jsonb)", NODE_TABLE)
            );
        }

        public static void setArguments(
            final @NotNull PreparedStatement statement,
            final @NotNull Node node
        ) throws SQLException {
            var parameterIndex = 1;

            statement.setLong(parameterIndex++, node.getId().longValue());
            statement.setInt(parameterIndex++, node.getVersion().intValue());
            statement.setTimestamp(
                parameterIndex++,
                Timestamp.from(
                    node
                        .getTimestamp()
                        .toGregorianCalendar()
                        .toZonedDateTime()
                        .toInstant()
                )
            );
            statement.setLong(parameterIndex++, node.getUid().longValue());
            statement.setLong(parameterIndex++, node.getChangeset().longValue());
            statement.setDouble(parameterIndex++, node.getLat());
            statement.setDouble(parameterIndex++, node.getLon());

            val tagsJson = JsonSerializer.serialize(node.getTag());
            statement.setString(parameterIndex, tagsJson.toString());
        }

        public static @NotNull PreparedStatement makeInsertUser(
            final @NotNull Connection connection
        ) throws SQLException {
            return connection.prepareStatement(
                String.format("insert into %s values(?, ?) on conflict do nothing", USER_TABLE)
            );
        }

        public static void setArguments(
            final @NotNull PreparedStatement statement,
            final long uid,
            final @NotNull String userName
        ) throws SQLException {
            var parameterIndex = 1;

            statement.setLong(parameterIndex++, uid);
            statement.setString(parameterIndex, userName);
        }
    }
}
