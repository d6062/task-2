package ru.nsu.mmichurov.dis.task2.db.inserters.customtypes;

import ru.nsu.mmichurov.dis.task2.db.inserters.INodeInserter;

import java.util.List;


public interface CustomTypeInserter extends INodeInserter {
    String NODE_TABLE = "osm_custom_types.node";
    String USER_TABLE = "osm_custom_types.user";

    @Override
    default Iterable<String> getTables() {
        return List.of(NODE_TABLE, USER_TABLE);
    }
}
