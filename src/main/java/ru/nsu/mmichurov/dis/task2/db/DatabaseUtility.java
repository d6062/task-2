package ru.nsu.mmichurov.dis.task2.db;

import lombok.experimental.UtilityClass;
import lombok.extern.log4j.Log4j2;
import lombok.val;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Scanner;

@UtilityClass
@Log4j2
public final class DatabaseUtility {
    private static final String[] SCHEMA_FILES = new String[]{
            "/db/schema/osm_schema_relations.sql",
            "/db/schema/osm_schema_json.sql",
            "/db/schema/osm_schema_custom_types.sql"
    };

    public static void createDatabaseStructure(final @NotNull Connection connection) throws IOException, SQLException {
        var autocommit = connection.getAutoCommit();
        connection.setAutoCommit(false);

        try {
            for (val schemaFile : SCHEMA_FILES) {
                createSchema(connection, schemaFile);
            }
        } catch (IOException | SQLException e) {
            connection.rollback();
            throw e;
        } finally {
            connection.setAutoCommit(autocommit);
        }
    }

    private static void createSchema(
            final @NotNull Connection connection,
            final @NotNull String schemaFile
    ) throws IOException, SQLException {
        try (
                val input = DatabaseUtility.class.getResourceAsStream(schemaFile);
                val statement = connection.createStatement()
        ) {
            log.debug(String.format("Creating schema \"%s\"", schemaFile));

            if (null == input) {
                log.error("Schema file not found");
                throw new IOException(String.format("Schema file \"%s\" not found", schemaFile));
            }

            try (val scanner = new Scanner(input)) {
                scanner.useDelimiter(";\r?\n?");
                while (scanner.hasNext()) {
                    val query = scanner.next().replaceAll("\r?\n", "\n");
                    log.debug(String.format("Executing \"%s\"", repr(query)));
                    statement.execute(query);
                }
            }

            log.debug("Created schema");
        }
    }

    public static @NotNull String escape(final @NotNull String s) {
        return s
                .replaceAll("\\\\", "\\\\\\\\")
                .replaceAll("\b", "\\b")
                .replaceAll("\n", "\\n")
                .replaceAll("\r", "\\r")
                .replaceAll("\t", "\\t")
                .replaceAll("\\x1A", "\\Z")
                .replaceAll("\\x00", "\\0")
                // evil stuff ahead
                .replaceAll("'", "")
                .replaceAll("\"", "")
                .replaceAll(",", "")
                .replaceAll("\\)", "")
                .replaceAll("\\(", "");
    }

    public static @NotNull String repr(final @NotNull String query) {
        val end = query.indexOf('\n');
        val result = query.substring(0, end > 0 ? end : query.length());

        if (end > 0) {
            return result + "...";
        }

        return result;
    }
}
