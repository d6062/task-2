package ru.nsu.mmichurov.dis.task2.db.inserters;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.openstreetmap.osm.Node;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.function.Function;

public interface INodeInserter {
    default void clean(final @NotNull Connection connection) throws SQLException {
        try (val statement = connection.createStatement()) {
            for (val table : getTables()) {
                statement.execute(String.format("delete from %s", table));
            }
        }
    }

    Iterable<String> getTables();

    long insert(
        @NotNull Iterable<Node> nodes,
        @NotNull Connection connection,
        @NotNull Function<Long, Boolean> shouldContinue
    ) throws SQLException;
}
