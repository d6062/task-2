package ru.nsu.mmichurov.dis.task2.db.inserters.customtypes;

import lombok.experimental.UtilityClass;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.openstreetmap.osm.Node;
import ru.nsu.mmichurov.dis.task2.db.inserters.customtypes.utility.CustomTypeSerializer;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.MessageFormat;
import java.util.function.Function;

public final class DefaultCustomTypeInserter implements CustomTypeInserter {
    @Override
    public long insert(
            final @NotNull Iterable<Node> nodes,
            final @NotNull Connection connection,
            final @NotNull Function<Long, Boolean> shouldContinue
    ) throws SQLException {
        var insertedCount = 0L;

        try (val statement = connection.createStatement()) {
            for (val node : nodes) {
                if (!shouldContinue.apply(insertedCount)) {
                    break;
                }

                insertNode(node, statement);
                insertedCount += 1;
            }
        }

        return insertedCount;
    }

    private static void insertNode(
            final @NotNull Node node,
            final @NotNull Statement statement
    ) throws SQLException {
        var query = QueryFormatter.makeInsert(node.getUid().longValue(), node.getUser(), USER_TABLE);
        statement.execute(query);

        val tagsCustomType = CustomTypeSerializer.serialize(node.getTag());
        query = QueryFormatter.makeInsert(node, tagsCustomType, NODE_TABLE);
        statement.execute(query);
    }

    @UtilityClass
    private static final class QueryFormatter {
        public static @NotNull String makeInsert(
                final @NotNull Node node,
                final @NotNull String tagsCustomType,
                final @NotNull String table
                ) {
            return MessageFormat.format(
                    "insert into {0} values ({1,number,#}, {2,number,#}, " +
                            "''{3}'', {4,number,#}, {5,number,#}, {6,number,#}, {7,number,#}, {8})",
                    table,
                    node.getId(), node.getVersion(), node.getTimestamp(),
                    node.getUid(), node.getChangeset(),
                    node.getLat(), node.getLon(),
                    tagsCustomType
            );
        }

        public static @NotNull String makeInsert(
                final long userId,
                final @NotNull String userName,
                final @NotNull String table
        ) {
            return MessageFormat.format(
                    "insert into {0} values ({1,number,#}, ''{2}'') on conflict do nothing",
                    table,
                    userId, userName
            );
        }
    }
}
