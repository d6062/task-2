package ru.nsu.mmichurov.dis.task2.db.inserters.customtypes.utility;

import lombok.AllArgsConstructor;
import org.openstreetmap.osm.Tag;

import static ru.nsu.mmichurov.dis.task2.db.DatabaseUtility.escape;

@AllArgsConstructor(staticName = "of")
public class TagView {
    private final Tag tag;

    @Override
    public String toString() {
        return String.format("('%s','%s')", escape(tag.getK()), escape(tag.getV()));
    }
}
