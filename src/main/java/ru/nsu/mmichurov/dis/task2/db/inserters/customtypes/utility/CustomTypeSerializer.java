package ru.nsu.mmichurov.dis.task2.db.inserters.customtypes.utility;

import lombok.experimental.UtilityClass;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.openstreetmap.osm.Tag;

import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@UtilityClass
public class CustomTypeSerializer {
    public static @NotNull String serialize(final @NotNull Iterable<Tag> tags) {
        val tagsArrayString = StreamSupport.stream(tags.spliterator(), false)
                .map(TagView::of)
                .map(TagView::toString)
                .collect(Collectors.joining(","));

        if (tagsArrayString.isEmpty()) {
            return "ARRAY[]::tag[]";
        }

        return "ARRAY[" + tagsArrayString + "]::tag[]";
    }
}
