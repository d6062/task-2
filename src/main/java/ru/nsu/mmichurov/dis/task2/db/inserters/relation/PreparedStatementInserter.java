package ru.nsu.mmichurov.dis.task2.db.inserters.relation;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.openstreetmap.osm.Node;
import org.openstreetmap.osm.Tag;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.function.Function;

public final class PreparedStatementInserter extends RelationInserter {
    private final long batchSize;
    private long currentBatchSize = 0;

    public PreparedStatementInserter(final long batchSize) {
        this.batchSize = batchSize;
    }

    public PreparedStatementInserter() {
        this(-1);
    }

    @Override
    public long insert(
        final @NotNull Iterable<Node> nodes,
        final @NotNull Connection connection,
        final @NotNull Function<Long, Boolean> shouldContinue
    ) throws SQLException {
        var insertedCount = 0L;

        try (
            val insertNode = StatementFormatter.makeInsertNode(connection);
            val insertTag = StatementFormatter.makeInsertTag(connection);
            val insertUser = StatementFormatter.makeInsertUser(connection)
        ) {
            for (val node : nodes) {
                if (!shouldContinue.apply(insertedCount)) {
                    break;
                }

                insertNode(node, insertNode, insertTag, insertUser);
                insertedCount += 1;

                handlePrepared(insertNode, insertTag, insertUser);
            }

            insertUser.executeBatch();
            insertNode.executeBatch();
            insertTag.executeBatch();
        }

        return insertedCount;
    }

    private void insertNode(
        final @NotNull Node node,
        final @NotNull PreparedStatement insertNode,
        final @NotNull PreparedStatement insertTag,
        final @NotNull PreparedStatement insertUser
    ) throws SQLException {
        StatementFormatter.setArguments(insertUser, node.getUid().longValue(), node.getUser());
        handlePrepared(insertUser);

        StatementFormatter.setArguments(insertNode, node);
        handlePrepared(insertNode);

        for (val tag : node.getTag()) {
            StatementFormatter.setArguments(insertTag, tag, node.getId().longValue());
            handlePrepared(insertTag);
        }
    }

    private void handlePrepared(final @NotNull PreparedStatement statement) throws SQLException {
        if (batchSize <= 0) {
            statement.execute();
            return;
        }

        statement.addBatch();
        currentBatchSize += 1;
    }

    private void handlePrepared(
        final @NotNull PreparedStatement insertNode,
        final @NotNull PreparedStatement insertTag,
        final @NotNull PreparedStatement insertUser
    ) throws SQLException {
        if (batchSize <= 0 || currentBatchSize < batchSize) {
            return;
        }

        insertUser.executeBatch();
        insertNode.executeBatch();
        insertTag.executeBatch();

        currentBatchSize = 0;
    }

    private static final class StatementFormatter {
        public static @NotNull PreparedStatement makeInsertNode(
            final @NotNull Connection connection
        ) throws SQLException {
            return connection.prepareStatement(
                String.format("insert into %s values(?, ?, ?, ?, ?, ?, ?)", NODE_TABLE)
            );
        }

        public static void setArguments(
            final @NotNull PreparedStatement statement,
            final @NotNull Node node
        ) throws SQLException {
            var parameterIndex = 1;

            statement.setLong(parameterIndex++, node.getId().longValue());
            statement.setInt(parameterIndex++, node.getVersion().intValue());
            statement.setTimestamp(
                parameterIndex++,
                Timestamp.from(
                    node
                        .getTimestamp()
                        .toGregorianCalendar()
                        .toZonedDateTime()
                        .toInstant()
                )
            );
            statement.setLong(parameterIndex++, node.getUid().longValue());
            statement.setLong(parameterIndex++, node.getChangeset().longValue());
            statement.setDouble(parameterIndex++, node.getLat());
            statement.setDouble(parameterIndex, node.getLon());
        }

        public static @NotNull PreparedStatement makeInsertTag(
            final @NotNull Connection connection
        ) throws SQLException {
            return connection.prepareStatement(String.format("insert into %s values(?, ?, ?)", TAG_TABLE));
        }

        public static void setArguments(
            final @NotNull PreparedStatement statement,
            final @NotNull Tag tag,
            final long nodeId
        ) throws SQLException {
            var parameterIndex = 1;

            statement.setLong(parameterIndex++, nodeId);
            statement.setString(parameterIndex++, tag.getK());
            statement.setString(parameterIndex, tag.getV());
        }

        public static @NotNull PreparedStatement makeInsertUser(
            final @NotNull Connection connection
        ) throws SQLException {
            return connection.prepareStatement(
                String.format("insert into %s values(?, ?) on conflict do nothing", USER_TABLE)
            );
        }

        public static void setArguments(
            final @NotNull PreparedStatement statement,
            final long uid,
            final @NotNull String userName
        ) throws SQLException {
            var parameterIndex = 1;

            statement.setLong(parameterIndex++, uid);
            statement.setString(parameterIndex, userName);
        }
    }
}
