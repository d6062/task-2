package ru.nsu.mmichurov.dis.task2.db.inserters;

import org.jetbrains.annotations.NotNull;

public enum InsertMethod {
    DEFAULT,
    PREPARED_STATEMENT,
    BATCH;

    @Override
    public @NotNull String toString() {
        return switch (this) {
            case DEFAULT -> "default";
            case PREPARED_STATEMENT -> "prepared-statement";
            case BATCH -> "batch";
        };
    }
}
