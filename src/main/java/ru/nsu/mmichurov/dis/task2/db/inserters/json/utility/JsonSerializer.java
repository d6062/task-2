package ru.nsu.mmichurov.dis.task2.db.inserters.json.utility;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;
import org.openstreetmap.osm.Tag;

import java.util.stream.StreamSupport;

@UtilityClass
public class JsonSerializer {
    public JSONArray serialize(final @NotNull Iterable<Tag> tags) {
        return new JSONArray()
                .putAll(
                        StreamSupport.stream(tags.spliterator(), false)
                                .map(JsonSerializer::serialize)
                                .toArray()
                );
    }

    public @NotNull JSONObject serialize(final @NotNull Tag tag) {
        return new JSONObject()
            .put("k", tag.getK())
            .put("v", tag.getV());
    }
}
