package ru.nsu.mmichurov.dis.task2.db.inserters;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import ru.nsu.mmichurov.dis.task2.Pair;
import ru.nsu.mmichurov.dis.task2.db.inserters.customtypes.DefaultCustomTypeInserter;
import ru.nsu.mmichurov.dis.task2.db.inserters.customtypes.PreparedStatementCustomTypeInserter;
import ru.nsu.mmichurov.dis.task2.db.inserters.json.PreparedStatementJsonInserter;
import ru.nsu.mmichurov.dis.task2.db.inserters.relation.DefaultInserter;
import ru.nsu.mmichurov.dis.task2.db.inserters.json.DefaultJsonInserter;
import ru.nsu.mmichurov.dis.task2.db.inserters.relation.PreparedStatementInserter;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

public final class NodeInserterFactory {
    private final Map<Pair<InsertMethod, SchemaType>, Supplier<INodeInserter>> inserters = new HashMap<>();

    public NodeInserterFactory(final long batchSize) {
        inserters.put(Pair.of(InsertMethod.DEFAULT, SchemaType.RELATIONS), DefaultInserter::new);
        inserters.put(Pair.of(InsertMethod.PREPARED_STATEMENT, SchemaType.RELATIONS), PreparedStatementInserter::new);
        inserters.put(Pair.of(InsertMethod.BATCH, SchemaType.RELATIONS), () -> new PreparedStatementInserter(batchSize));

        inserters.put(Pair.of(InsertMethod.DEFAULT, SchemaType.JSON), DefaultJsonInserter::new);
        inserters.put(Pair.of(InsertMethod.PREPARED_STATEMENT, SchemaType.JSON), PreparedStatementJsonInserter::new);
        inserters.put(Pair.of(InsertMethod.BATCH, SchemaType.JSON), () -> new PreparedStatementJsonInserter(batchSize));

        inserters.put(Pair.of(InsertMethod.DEFAULT, SchemaType.CUSTOM_TYPES), DefaultCustomTypeInserter::new);
        inserters.put(Pair.of(InsertMethod.PREPARED_STATEMENT, SchemaType.CUSTOM_TYPES), PreparedStatementCustomTypeInserter::new);
        inserters.put(Pair.of(InsertMethod.BATCH, SchemaType.CUSTOM_TYPES), () -> new PreparedStatementCustomTypeInserter(batchSize));
    }

    public @NotNull INodeInserter getInserter(
        final @NotNull InsertMethod method,
        final @NotNull SchemaType schemaType
    ) throws InserterNotImplementedException {
        val inserterKey = Pair.of(method, schemaType);

        if (!inserters.containsKey(inserterKey)) {
            throw new InserterNotImplementedException(method, schemaType);
        }

        return inserters.get(inserterKey).get();
    }

    public static final class InserterNotImplementedException extends Exception {
        public InserterNotImplementedException(
            final @NotNull InsertMethod method,
            final @NotNull SchemaType schemaType
        ) {
            super(String.format("No suitable inserter for schema type \"%s\" and method \"%s\"", schemaType, method));
        }
    }
}
