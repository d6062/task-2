package ru.nsu.mmichurov.dis.task2.db.inserters;

public enum SchemaType {
    RELATIONS,
    JSON,
    CUSTOM_TYPES;

    @Override
    public String toString() {
        return switch (this) {
            case RELATIONS -> "relations";
            case JSON -> "json";
            case CUSTOM_TYPES -> "custom-types";
        };
    }
}
