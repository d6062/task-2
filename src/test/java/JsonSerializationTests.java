import lombok.val;
import org.junit.jupiter.api.Test;
import org.openstreetmap.osm.Tag;
import ru.nsu.mmichurov.dis.task2.db.inserters.json.utility.JsonSerializer;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class JsonSerializationTests {
    @Test
    public void Tag_is_properly_serialized() {
        // Arrange
        val tag = new Tag();
        tag.setK("Location");
        tag.setV("Anor Londo");

        // Act
        var json = JsonSerializer.serialize(tag);

        // Assert
        assertThat(json)
            .matches(it -> "Location".equals(it.getString("k")))
            .matches(it -> "Anor Londo".equals(it.getString("v")));
    }
}
